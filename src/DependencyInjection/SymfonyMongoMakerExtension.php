<?php

namespace Ama\SymfonyMongoMakerBundle\DependencyInjection;

use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;


class SymfonyMongoMakerExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        //dd('DI Load');
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
    }


}